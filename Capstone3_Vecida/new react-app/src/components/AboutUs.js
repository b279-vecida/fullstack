import React from 'react';
import teumelogo from '../images/teumelogo.PNG'; 

const AboutUs = () => {
  return (
 <div className="about-us container-fluid py-md-5" >
      <div className="row justify-content-center py-md-5 my-md-5">
        <div className="col-md-3 my-5 my-md-0" img src={teumelogo} alt="Logo" className="img-fluid" >
          
        </div>
        <div className="col-md-7">
        <h2> About Us</h2>
          <h4 className="mt-md-5 pb-3">
            Hello TreasureMakers!
          </h4>
          <p>
            Welcome to the world of K-pop idol TREASURE! We are dedicated to showcasing the incredible talent, passion, and dedication of these extraordinary artists.
          </p>
          <p>
            These idols not only excel in their artistic abilities but also serve as role models, inspiring millions of fans with their hard work, perseverance, and positive influence. They often engage in philanthropic activities, spread messages of love and unity, and actively participate in charitable causes.
          </p>
        </div>
      </div>
    </div>
  );
}

export default AboutUs;
