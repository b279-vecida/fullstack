import { Row, Col, Card } from "react-bootstrap";

export default function HotDeals(){

	return(
		<Row className="my-3">
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>Receive Your Parcel at Home</h2>
                        </Card.Title>
                    </Card.Header>
                        <Card.Text>
                             is a convenient service that allows individuals to have their parcels and packages delivered directly to their residential address. With this service, recipients no longer need to visit post offices or collection points to retrieve their items. Instead, the delivery personnel will bring the package right to the doorstep of the recipient's home.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                    <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>Buy Now, Pay Later</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text>
                            is a flexible payment option that allows customers to make purchases and defer payment for a later date. This service enables individuals to buy products or services immediately, even if they don't have the full payment amount available at the time of purchase. Instead of paying the entire cost upfront, customers have the option to divide the total amount into smaller, manageable installments spread over a specific period of time.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight">
                    <Card.Body>
                        <Card.Header className="th-bg p-3 my-3">
                        <Card.Title>
                            <h2>Easy Pay, with your E-Wallet</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text>
                           refers to a simplified and convenient method of making payments using an electronic wallet or e-wallet. An e-wallet is a digital payment system that allows individuals to store their payment information, such as credit or debit card details, bank account information, or digital currencies, in a secure virtual wallet accessible through a mobile app or online platform.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
	)
}