import React from 'react';

const Footer = () => {
  return (
    <footer>
      <div className="container text-center">
        <p>&copy; {new Date().getFullYear()} TEUME. All rights reserved.</p>
      </div>
    </footer>
  );
}

export default Footer;