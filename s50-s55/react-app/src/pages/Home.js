import Banner from "../components/Banner";
import Highlights from "../components/Highlights";


export default function Home(){

	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities everyone, everywhere!",
		destination: "/course",
		label: "Enroll Now!"
	}



	return(
			<>
			<Banner bannerProps={data}/>
			<Highlights/>
	
			</>


		)
}