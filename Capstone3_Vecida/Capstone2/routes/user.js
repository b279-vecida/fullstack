const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");
const productController = require("../controllers/productController.js");

// ==========================Route for checking if the user's email already exists in the database=================================
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

router.get("/all", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllUsers(req.body,isAdmin).then(resultFromController => res.send(resultFromController));
});

//=======================================ROUTE FOR USER REGISTRATION===================================================
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//========================================ROUTE FOR USER REGISTRATION======================================================
router.post("/login", (req, res) => {
    console.log(req.body)
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


//====================================ROUTE FOR RETRIEVING USER DETAILS============================================
router.post("/details", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);

    userController.getProfile(userData.id).then(resultFromController => res.send(resultFromController));
})

/*router.post("/checkout", (req, res) => {
    let data = {
        product: req.body,
        userId: auth.decode(req.headers.authorization).id,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    userController.createOrder(data).then(resultFromController => res.send(resultFromController))
})
*/
//======================================== ROUTE FOR ORDER (CHECKOUT) ===================================================================
router.post("/checkout", auth.verify, (req, res,) => {
    let data = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        userId : auth.decode(req.headers.authorization).id,
        product : req.body
    }
        userController.checkout(data).then(resultFromController => res.send(resultFromController));
});

router.put("/:userId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.archiveUser(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	
});

// =======================================ROUTE FOR RETRIEVING USER DETAILS ==============================================================

/*router.post("/:userId/userDetails", auth.verify, (req,res) => {
	let userData = auth.decode(req.headers.authorization).userData;
	console.log(userData);

	userController.checkout(req.params,userData).then(resultFromController => res.send(resultFromController));
})
*/




// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;

