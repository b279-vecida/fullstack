import {Carousel, Image} from 'react-bootstrap';

export default function HomeCarousel(){
	return (
			/* Home Carousel */
		  <div>
		    <Carousel className='vh-75 w-100 d-inline-block mb-5 shadow' fade>
		    <Carousel.Item>
		        <Image
		        className="d-block mx-auto img-fit  carousel-height carousel-caption-bg"
		        src="https://i.pinimg.com/564x/2d/ab/15/2dab15911cee811ae2de38c75bdcaa2f.jpg"
		        alt="First slide"
		        />
		        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25 '>
		        <div  className='carousel-text'>
		        <h1>Get our new TEUM Collection!</h1>
		        <p>Buy the latest Treasure Official Merch!</p>
		        </div>
		       </Carousel.Caption>
		    </Carousel.Item>

		    <Carousel.Item>
	        <Image
	        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
	        src="https://i.pinimg.com/564x/11/ff/b0/11ffb07655bbf162057b90d4112c18d4.jpg"
	        alt="Second slide"
	        />
	        <Carousel.Caption className='container d-flex flex-column align-items-center justify-content-center vh-25'>
	        <div  className='carousel-text'>
	        <h1>See new merch of TRUZ</h1>
	        <p>Go get your favorite truz character!</p>
	        </div>
	        </Carousel.Caption>
	    		</Carousel.Item>

	    	<Carousel.Item>
	        <Image
	        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
	        src="https://i.pinimg.com/564x/7d/92/85/7d92859f7637191062628d26eb53058a.jpg"
	        alt="Third slide"
	        />
	    	</Carousel.Item>
	   		</Carousel>

		    </div>

		)
}