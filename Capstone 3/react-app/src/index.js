import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


// Importing react-bootstrap in order to activate "bootstrap" in the browser.
// npm install react-bootstrap bootstrap
import "bootstrap/dist/css/bootstrap.min.css";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(


  <React.StrictMode>
    <App />
  </React.StrictMode>
);



