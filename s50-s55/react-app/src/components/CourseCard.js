import { Card, Button } from 'react-bootstrap';


//In React.js we have 3 hooks
/*
1. useState
2. useEffect
3. useContext


*/

import { useState } from "react"
import { Link } from "react-router-dom";

export default function CourseCard({courseProp}) {
    //Checks if props was successfully passed
    console.log(courseProp.name);
    //Check the type of passed data
    console.log(typeof courseProp);

    //=====Destructuring the courseProp into their own variables====
    const { _id,name, description, price } = courseProp;

    // useState
    // Use for storing different states
    // used to keep track of information to individual components

    //SYNTAX -> const [getter, setter] = useState(initialGetterValue);

  //   const [count, setCount] = useState(0);
  //   console.log(useState(0));


  //   const [seat, setSeat] = useState(30);



  //   // Function that keeps track of the enrollees for a course
  // /*  function enroll(){
  //       setCount(count + 1);

  //       console.log("Enrollees" + count);

  //       // Will prompt if the seats reaches 30
  //       if(count >= 30){
  //           alert("No more seats!");
  //       }
  //   }*/
  //       function enroll(){
  //       if(seat > 0){
  //           setCount(count + 1);
  //           setSeat(seat - 1);
  //       }else{
  //           alert("No more seats available!");
  //       }
        
  //       console.log("Enrollees: " + count);
  //       console.log("Available Seats: " + seat);
  //   }
 
    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{courseProp.description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                {/*<Card.Subtitle>Enrollees:</Card.Subtitle>
                <Card.Text>{count}</Card.Text>*/}
                {/*<Button variant="primary" onClick={enroll}>Enroll</Button>*/}
                <Link className="btn btn-primary" to={`/courseView/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}