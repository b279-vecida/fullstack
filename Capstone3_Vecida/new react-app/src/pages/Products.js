import { useEffect, useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import HomeBanner from "../components/HomeBanner";
import ProductCard from "../components/ProductCard";
import UserContext from "../UserContext";
import logo from "../images/teulogo.png";
import { Row } from "react-bootstrap";
import FeaturedProducts from "../components/FeaturedProducts";

export default function Products() {
  const data = {
    title: "TEUME",
    content:
      "This is a fanmade website which consists of Treasure Merchandise! ",
    destination: "/products",
    label: "Buy Now!",
    image: { logo },
  };

  const { user } = useContext(UserContext);

  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch("http://localhost:4000/products/")
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts(
          data.map((products) => {
            return <ProductCard key={products._id} props={products} />;
          })
        );
      });
  }, []);

  return user.isAdmin ? (
    <Navigate to="/dashboard" />
  ) : (
    <>
      <div className="p-5">
       
        <h1 className="product-1 text-center">TEUME MERCH PRODUCTS</h1>
        <Row className="my-3 text-dark h-100">{products}</Row>
      </div>
    </>
  );
}
