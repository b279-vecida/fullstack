const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        
    },
    lastName : {
        type : String,
        
    },
    email: {
        type: String,
        
        },
    password: {
        type: String,
        
        
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo : {
        type : String, 
        
    },
    orderedProduct: [{
        products: [{
            productId: {
                type: String,
                required: [true, "Is required"]
            },
            productName: {
                type: String,
                retuired: [true, "Is required"]
            },
            quantity: {
                type: Number,
                required: [true, "Is required"]
            }
        }],
        totalAmount: {
            type: Number,
            required: [true, "Is required"]
        },
        PurchasedOn: {
            type: Date,
            default: new Date()
        }
    }]
})

module.exports = mongoose.model('User', userSchema)