import { Navigate } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext";
import HomeBanner from "../components/HomeBanner";
import HotDeals from "../components/HotDeals";
import HomeCarousel from "../components/HomeCarousel";
import logo from "../images/teulogo.png"
import FeaturedProducts from "../components/FeaturedProducts";

export default function Home(){

	
	const { user } = useContext(UserContext);
	const data = {
		title: "TEUME Philippines",
		content: "A fanmade website for treasuremakers mechandise!",
		destination: "/products",
		label: "Shop Now!",
		image: {logo}
	}

	return(
		<>
		{
        (user.isAdmin)
		?
		<Navigate to="/dashboard" />
		:
		<div className="p-5">
			<HomeCarousel/>
			<HomeBanner bannerProp={data}/>
			<HotDeals />
			<FeaturedProducts/>
		</div>
		}
		</>
		
	)
}