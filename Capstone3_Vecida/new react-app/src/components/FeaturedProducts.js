import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function FeaturedProducts() {
    return (
        <div className='d-flex flex-column my-5 banner-bg p-5 text-white '>
        <h2 className="text-center"> CHECK OUT OUR HELLO TOUR MERCHANDISE! </h2>
        <Row className="my-3 text-dark h-100">
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height" >
                <Card.Img  variant="top" src="https://cdn-contents.weverseshop.io/public/shop/c7ec207d5ecd96285a8f1b49262964cc.jpg?q=95&w=320" />
                <Card.Header>
                <Card.Title> [HELLO] Treasure Official LightStick</Card.Title>
                </Card.Header>
            
                <Card.Footer className='text-center'>
                <Button className="w-50 custom-button" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src="https://cdn-contents.weverseshop.io/public/shop/a4e439305c4d63167e5838a1d93b0365.png?q=95&w=320" />
                <Card.Header>
                    <Card.Title>[HELLO] Treasure Hoodie Design 2</Card.Title>
                    </Card.Header>

                <Card.Footer className='text-center'>
                <Button className="w-50 custom-button" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src="https://cdn-contents.weverseshop.io/public/shop/dc4727aba66f519143266391a0e3561a.png?q=95&w=320" />
                <Card.Header>
                    <Card.Title>[HELLO] Treasure T-Shirt Design 2</Card.Title>
                    </Card.Header>
          
                <Card.Footer className='text-center'>
                <Button className="w-50 custom-button" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={6} lg={3}>
                <Card className="card-height">
                <Card.Img variant="top" src="https://cdn-contents.weverseshop.io/public/shop/1e42808733fcbc5b2706653a90ea0237.png?q=95&w=320" />
                <Card.Header>
                    <Card.Title>[HELLO] Treasure Trading Photocard Set </Card.Title>
                    </Card.Header>
           
                <Card.Footer className='text-center'>
                <Button className="w-50 custom-button" as = {Link} to={"/products"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
        </Row>
        </div>



    );
}
